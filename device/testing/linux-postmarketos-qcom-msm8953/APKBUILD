# Maintainer: M0Rf30 <morf3089@gmail.com>
# Co-Maintainer: Ultra-azu <ultra.public@proton.me>
# Kernel config based on: defconfig, msm8953.config, and device configs

_flavor="postmarketos-qcom-msm8953"
pkgname=linux-$_flavor
pkgver=6.4.0
pkgrel=0
pkgdesc="Close to mainline linux kernel for Qualcomm Snapdragon MSM8953"
arch="aarch64"
_carch="arm64"
url="https://github.com/msm8953-mainline/linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-community
"
makedepends="
	bash
	bison
	findutils
	flex
	openssl-dev
	perl
	postmarketos-installkernel
"

# Source
_commit="6c0b09872649fc6a2619f8cc504996fbf2722a77"

source="
	$pkgname-$_commit.tar.gz::https://github.com/msm8953-mainline/linux/archive/$_commit.tar.gz
	config-$_flavor.aarch64
"
builddir="$srcdir/linux-$_commit"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$arch" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-$_flavor"
}

package() {
	mkdir -p "$pkgdir"/boot

	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir/boot/dtbs"
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -Dm644 "$builddir/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"
}

sha512sums="
519857e38f14934b4963ff43b6f19c01d73d03aa7853f155b1d8c605782ca3762fd915ccedd57eddae0fdc1186b6fd28c4226d3f1b510cecc45bda8b86adbdb7  linux-postmarketos-qcom-msm8953-6c0b09872649fc6a2619f8cc504996fbf2722a77.tar.gz
f93ac67ec59998e0d2ed5f167bdb55b5d2fa66a9479d4a84c7094bac3af39fd4235d4ce2f6d3b167be95a6fd6c216b4a6d31b5cfe9282401c6b0f5cf9d676c8a  config-postmarketos-qcom-msm8953.aarch64
"
